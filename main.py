import uuid
import logging

import uvicorn
from sqlalchemy import select
from fastapi import FastAPI
from starlette.responses import JSONResponse
from sqlalchemy.ext.asyncio import AsyncSession

from schemas import CalcPostRequest
from model import CalculateHistory, Base
from database import engine

# init FastAPI app
app = FastAPI()

# Инициализируем логгер
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

session = AsyncSession(engine)

async def init_models():
    """
    Инициализация моделей базы данных.
    """
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)


# defining exceptional JSON-response for incorrect DB queries
@app.exception_handler(Exception)
async def error_handler(request, exc):
    """
    Обработчик исключений для FastAPI при ошибке.
    """
    logger.error(f'Error: {exc}', exc_info=True)
    return JSONResponse({
        'detail': f'{exc}'
    })

@app.on_event("startup")
async def startup():
    """
    Обработчик события при запуске приложения.
    """
    logger.info("Starting the application...")
    await init_models()
    logger.info("Application initialization completed.")
    
# showing history of math operations
@app.get('/history', summary='Get history of operations')
async def get_history():
    """
    Получение истории математических операций.
    """
    stmt = select(CalculateHistory)
    result = await session.execute(stmt)
    return result.scalars().all()

#showing math operation and a result
@app.get('/calc/{uid}', summary='Get history of operations')
async def get_task_by_id(uid):
    """
    Получение математической операции по её уникальному идентификатору (UID).
    """
    stmt = select(CalculateHistory).where(CalculateHistory.id == uid)
    result = await session.execute(stmt)
    return result.scalars().first()

#create math operation
@app.post('/calc', summary='Calc as post method')
async def post_calc(body: CalcPostRequest):
    """
    Создание математической операции и вызов задачи сохранения её в базу данных.
    """
    params = body.model_dump()
    uid = str(uuid.uuid4())
    await calculate(uid, params)
    return {'uid': uid}

# saving math operation to DB
async def calculate(id, params):
    """
    Выполнение математической операции и сохранение её в базу данных.
    """
    response = {'result': '', 'operation': '', 'uid': id}
    if params['operator'] == '*':
        response['result'] = params['first'] * params['last']
        response['operation'] = str(
            params['first']) + '*' + str(params['last'])

    elif params['operator'] == '/':
        response['result'] = params['first'] / params['last']
        response['operation'] = str(
            params['first']) + '/' + str(params['last'])

    elif params['operator'] == '+':
        response['result'] = params['first'] + params['last']
        response['operation'] = str(
            params['first']) + '+' + str(params['last'])

    elif params['operator'] == '-':
        response['result'] = params['first'] - params['last']
        response['operation'] = str(
            params['first']) + '-' + str(params['last'])
        
    logger.info(f"Operation {response['operation']} calculated with result {response['result']}")
    
    saved_operation = CalculateHistory(
        id = response['uid'],
        operation=str(
        response['operation']), 
        result=str(response['result']))
    
    session.add(saved_operation)
    await session.commit()
    
    
if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)