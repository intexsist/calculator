from pydantic import BaseModel, validator

class CalcPostRequest(BaseModel):
    first: int
    last: int
    operator: str
    @validator('operator')
    def onlySimpleMath(cls, v):
        if v == '*' or v == '/' or v == '-' or v == '+':
            return v
        raise ValueError('only simple math requiert')


class CalcResponce(BaseModel):
    uid: str = None
    result: int = None
    operation: str = None