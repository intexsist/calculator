from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker

from model import CalculateHistory

engine = create_async_engine('sqlite+aiosqlite:////projects/test/calculator/sqlite_python.db')


SessionLocal = sessionmaker(bind=engine, class_=AsyncSession)

