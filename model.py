import datetime

from sqlalchemy import DateTime
from sqlalchemy.sql import func
from sqlalchemy.orm import Mapped, mapped_column
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class CalculateHistory(Base):
    __tablename__ = "calculate_history"
    id: Mapped[str] = mapped_column(primary_key=True)
    operation: Mapped[int] = mapped_column() #* добавить max length
    result: Mapped[int] = mapped_column()
    created_date: Mapped[datetime.datetime] = mapped_column(
        DateTime(timezone=True), server_default=func.now()
    )