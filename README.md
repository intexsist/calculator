FastAPI-Calculator
The simple calculator based on FastAPI framework

What you need for running the App
Git, Python 3

How to get and run the App
Clone the App repository git clone https://gitlab.com/intexs/calculator.git

Enter to the 'FastAPI-Calculator' directory cd FastAPI-Calculator

Create venv 'python -m venv venv'

Activate venv

Install all needed libraries 'pip install -r requirements'

You can run ASGI server with working App uvicorn main:app --reload
Or run main.py

Also, you can testing the App using PyTest as in the comand bellow pytest -v